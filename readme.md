# Purpose

Xmas fun based on a [Facebook comment](https://www.facebook.com/groups/pythoncodingprogrammingselfinstructionhub/posts/2793028647512784/?__cft__[0]=AZXvnZwQ9k9pprNfBQ5IKGlifrU0sDgjWBMuvodG5IFqq8DhsurovzFnvxho56_JeqQ2LhKr4drbIa3gLZq3nI8TNEcITh1eQXea66T1K5bXYlcD7uRZmqa_7qWgz8a3leJRx0saqPx3Fwx2evXr5JTx&__tn__=%2CO%2CP-R) where someone posted a rather basic xmas tree generator script and I felt the need to expand on that... 

![Code Screenshot](screenshot-code.png)

Well... Going a *little* overboard...

But! It is customizable, contains recursion and everything gets logged and thus, it is a highly professional tool!

**Enjoy!**

## Usage

    usage: python xmas.py [-h] [-w N] [-b N] [-t N] [-m MSG]

    Generates a nice customizable christmas tree.

    optional arguments:
      -h, --help          show this help message and exit
      -w N, --width N     set screen width to N charachters for centering. It defaults to the current
                          width of the terminal.
      -b N, --branches N  set number of rows for branches to N
      -t N, --trunk N     set number of rows for trunk to N
      -m MSG, --msg MSG   set xmas message to MSG

    Ho ho ho! 🎅

## Docker

Build the container image using this command:

    docker build -t xmas:latest .

or

    podman build -t xmas:latest .

## Examples

### Example 1

Calling the programm with this command line

    python xmas.py -b 10 -t 1 -m "καλά Χριστούγεννα"

or with docker

    docker run xmas -b 10 -t 1 -m "καλά Χριστούγεννα"

or with podman

    podman run xmas -b 10 -t 1 -m "καλά Χριστούγεννα"

will result in this output

![Example Output](screenshot-1.png)

### Example 2

Calling the programm with this command line

    python xmas.py -w 30 -b 14 -t 2 -m "Frohe Weihnachten"

or with docker

    docker run xmas -w 30 -b 14 -t 2 -m "Frohe Weihnachten"

or with podman

    podman run xmas -w 30 -b 14 -t 2 -m "Frohe Weihnachten"

will result in this output

![Example Output](screenshot-2.png)
