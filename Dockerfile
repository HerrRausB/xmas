FROM ubuntu:22.04

RUN DEBIAN_FRONTEND=noninteractive apt update \
    && apt upgrade -y \
    && apt install -y python3

WORKDIR /app
COPY *.md *.py /app/
ENTRYPOINT [ "python3", "xmas.py"]