import argparse
from syslog import syslog
from os.path import sep as path_seperator
from os import get_terminal_size


# docker terminal has no default size
try:
    def_screen_width = get_terminal_size().columns
except OSError:
    def_screen_width = 80

def_branches_height = 12
def_trunk_height = 2
def_xmas_msg = "Merry Xmas"

character_star = '\u2605'
character_branch = '\u25b2'
character_trunk = '\u2588'
character_santa = '\u00f0\u009f\u008e\u0085'.encode("latin-1").decode("utf-8")

escape_color_branches = 92
escape_color_trunk = 91
escape_color_xmas_msg = 33

def log (msg):
    syslog (f"Xmas Tree {msg}")

def print_centered (screen_width = def_screen_width, text = "", escape_color=0):
    print (f"{int((screen_width-len(text))/2)*' '}\033[{escape_color}m{text}\033[0m")

def print_branches (screen_width = def_screen_width, branches_height = def_branches_height, curr_branches_row = 0):
    
    curr_branches_row = curr_branches_row + 1 if curr_branches_row > 0 else 1

    if curr_branches_row <= branches_height:
        log (f"printing branches row #{curr_branches_row}")
        branch_row_width = curr_branches_row if curr_branches_row%2 else curr_branches_row+1
        if curr_branches_row == 1:
            print_centered(screen_width=screen_width, text=character_star, escape_color=escape_color_xmas_msg)
        print_centered(screen_width=screen_width, text=branch_row_width*(character_branch), escape_color=escape_color_branches)
        print_branches(screen_width=screen_width,branches_height=branches_height,curr_branches_row=curr_branches_row)

def print_trunk (screen_width=def_screen_width, trunk_height=def_trunk_height):
    for trunk_row in range(trunk_height):
        log (f"printing trunk row #{trunk_row+1}")
        print_centered(screen_width=screen_width,text=character_trunk, escape_color=escape_color_trunk)

def print_xmas_msg (screen_width=def_screen_width,xmas_msg=def_xmas_msg):
        log (f"printing message '{xmas_msg}'")
        print_centered(screen_width=screen_width,text=xmas_msg,escape_color=escape_color_xmas_msg)


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(
                                    prog=f"python {__file__.split(path_seperator)[-1]}",
                                    description="Generates a nice customizable christmas tree.",
                                    epilog=f"Ho ho ho! {character_santa}")
        parser.add_argument('-w', 
                            '--width',
                            metavar='N',
                            type=int,
                            #action='store_true', 
                            help='set screen width to N charachters for centering. It defaults to the current width of the terminal.',
                            default = def_screen_width)
        parser.add_argument('-b', 
                            '--branches',
                            metavar='N',
                            type=int,
                            #action='store_true', 
                            help='set number of rows for branches to N',
                            default=def_branches_height)
        parser.add_argument('-t', 
                            '--trunk',
                            metavar='N',
                            type=int,
                            #action='store_true', 
                            help='set number of rows for trunk to N',
                            default=def_trunk_height)
        parser.add_argument('-m', 
                            '--msg',
                            metavar='MSG',
                            type=str,
                            #action='store_true', 
                            help='set xmas message to MSG',
                            default=def_xmas_msg)
        args = parser.parse_args()

        log (f"started with {args.width} characters screen width and height of {args.branches + args.trunk} rows")    

        print("")
        print_branches(screen_width=args.width,branches_height=args.branches)
        print_trunk(screen_width=args.width,trunk_height=args.trunk)
        print("")
        print_xmas_msg(screen_width=args.width,xmas_msg=args.msg)
        print("")
        log ("completed")
    except Exception as X:
        log (f"couldn't be completed due to '{X}'")
